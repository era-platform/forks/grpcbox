.PHONY: all grpc_gen

all: compile

compile:
	rm -f rebar.lock
	rebar3 compile

grpc_gen:
	rebar3 grpc gen

clean:
	rebar3 clean
distclean:
	rm -f TEST-*.xml
	rm -rf _build rebar.lock

list-deps:
	rebar3 deps
eunit:
	rebar3 eunit
xref:
	rebar3 xref
dialyzer:
	rebar3 dialyzer

cc: clean compile

ccx: clean compile xref eunit
