%%%-------------------------------------------------------------------
%% @doc Client module for grpc service smartspeech.synthesis.v1.SmartSpeech.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:20+00:00 and should not be modified manually

-module(grpcbox_sber_tts_synthesis_client).

-compile(export_all).
-compile(nowarn_export_all).

-include_lib("grpcbox/include/grpcbox.hrl").

-define(is_ctx(Ctx), is_tuple(Ctx) andalso element(1, Ctx) =:= ctx).

-define(SERVICE, 'smartspeech.synthesis.v1.SmartSpeech').
-define(PROTO_MODULE, 'grpcbox_synthesis_pb').
-define(MARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:encode_msg(I, T) end).
-define(UNMARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:decode_msg(I, T) end).
-define(DEF(Input, Output, MessageType), #grpcbox_def{service=?SERVICE,
                                                      message_type=MessageType,
                                                      marshal_fun=?MARSHAL_FUN(Input),
                                                      unmarshal_fun=?UNMARSHAL_FUN(Output)}).

%% @doc 
-spec synthesize(grpcbox_synthesis_pb:synthesis_request()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
synthesize(Input) ->
    synthesize(ctx:new(), Input, #{}).

-spec synthesize(ctx:t() | grpcbox_synthesis_pb:synthesis_request(), grpcbox_synthesis_pb:synthesis_request() | grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
synthesize(Ctx, Input) when ?is_ctx(Ctx) ->
    synthesize(Ctx, Input, #{});
synthesize(Input, Options) ->
    synthesize(ctx:new(), Input, Options).

-spec synthesize(ctx:t(), grpcbox_synthesis_pb:synthesis_request(), grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
synthesize(Ctx, Input, Options) ->
    grpcbox_client:stream(Ctx, <<"/smartspeech.synthesis.v1.SmartSpeech/Synthesize">>, Input, ?DEF(synthesis_request, synthesis_response, <<"smartspeech.synthesis.v1.SynthesisRequest">>), Options).

%% @doc Unary RPC
-spec async_synthesize(grpcbox_synthesis_pb:async_synthesis_request()) ->
    {ok, grpcbox_synthesis_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
async_synthesize(Input) ->
    async_synthesize(ctx:new(), Input, #{}).

-spec async_synthesize(ctx:t() | grpcbox_synthesis_pb:async_synthesis_request(), grpcbox_synthesis_pb:async_synthesis_request() | grpcbox_client:options()) ->
    {ok, grpcbox_synthesis_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
async_synthesize(Ctx, Input) when ?is_ctx(Ctx) ->
    async_synthesize(Ctx, Input, #{});
async_synthesize(Input, Options) ->
    async_synthesize(ctx:new(), Input, Options).

-spec async_synthesize(ctx:t(), grpcbox_synthesis_pb:async_synthesis_request(), grpcbox_client:options()) ->
    {ok, grpcbox_synthesis_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
async_synthesize(Ctx, Input, Options) ->
    grpcbox_client:unary(Ctx, <<"/smartspeech.synthesis.v1.SmartSpeech/AsyncSynthesize">>, Input, ?DEF(async_synthesis_request, task, <<"smartspeech.synthesis.v1.AsyncSynthesisRequest">>), Options).

