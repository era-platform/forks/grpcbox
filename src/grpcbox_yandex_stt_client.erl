%%%-------------------------------------------------------------------
%% @doc Client module for grpc service speechkit.stt.v3.Recognizer.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:16+00:00 and should not be modified manually

-module(grpcbox_yandex_stt_client).

-compile(export_all).
-compile(nowarn_export_all).

-include_lib("grpcbox/include/grpcbox.hrl").

-define(is_ctx(Ctx), is_tuple(Ctx) andalso element(1, Ctx) =:= ctx).

-define(SERVICE, 'speechkit.stt.v3.Recognizer').
-define(PROTO_MODULE, 'grpcbox_yandex_speechkit_stt_service_pb').
-define(MARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:encode_msg(I, T) end).
-define(UNMARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:decode_msg(I, T) end).
-define(DEF(Input, Output, MessageType), #grpcbox_def{service=?SERVICE,
                                                      message_type=MessageType,
                                                      marshal_fun=?MARSHAL_FUN(Input),
                                                      unmarshal_fun=?UNMARSHAL_FUN(Output)}).

%% @doc 
-spec recognize_streaming() ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
recognize_streaming() ->
    recognize_streaming(ctx:new(), #{}).

-spec recognize_streaming(ctx:t() | grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
recognize_streaming(Ctx) when ?is_ctx(Ctx) ->
    recognize_streaming(Ctx, #{});
recognize_streaming(Options) ->
    recognize_streaming(ctx:new(), Options).

-spec recognize_streaming(ctx:t(), grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
recognize_streaming(Ctx, Options) ->
    grpcbox_client:stream(Ctx, <<"/speechkit.stt.v3.Recognizer/RecognizeStreaming">>, ?DEF(streaming_request, streaming_response, <<"speechkit.stt.v3.StreamingRequest">>), Options).

