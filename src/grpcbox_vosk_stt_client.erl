%%%-------------------------------------------------------------------
%% @doc Client module for grpc service vosk.stt.v1.SttService.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:11+00:00 and should not be modified manually

-module(grpcbox_vosk_stt_client).

-compile(export_all).
-compile(nowarn_export_all).

-include_lib("grpcbox/include/grpcbox.hrl").

-define(is_ctx(Ctx), is_tuple(Ctx) andalso element(1, Ctx) =:= ctx).

-define(SERVICE, 'vosk.stt.v1.SttService').
-define(PROTO_MODULE, 'grpcbox_vosk_stt_service_pb').
-define(MARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:encode_msg(I, T) end).
-define(UNMARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:decode_msg(I, T) end).
-define(DEF(Input, Output, MessageType), #grpcbox_def{service=?SERVICE,
                                                      message_type=MessageType,
                                                      marshal_fun=?MARSHAL_FUN(Input),
                                                      unmarshal_fun=?UNMARSHAL_FUN(Output)}).

%% @doc 
-spec streaming_recognize() ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
streaming_recognize() ->
    streaming_recognize(ctx:new(), #{}).

-spec streaming_recognize(ctx:t() | grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
streaming_recognize(Ctx) when ?is_ctx(Ctx) ->
    streaming_recognize(Ctx, #{});
streaming_recognize(Options) ->
    streaming_recognize(ctx:new(), Options).

-spec streaming_recognize(ctx:t(), grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
streaming_recognize(Ctx, Options) ->
    grpcbox_client:stream(Ctx, <<"/vosk.stt.v1.SttService/StreamingRecognize">>, ?DEF(streaming_recognition_request, streaming_recognition_response, <<"vosk.stt.v1.StreamingRecognitionRequest">>), Options).

