%%%-------------------------------------------------------------------
%% @doc Behaviour to implement for grpc service speechkit.stt.v3.Recognizer.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:16+00:00 and should not be modified manually

-module(grpcbox_yandex_stt_bhvr).

%% @doc 
-callback recognize_streaming(reference(), grpcbox_stream:t()) ->
    ok | grpcbox_stream:grpc_error_response().

