%%%-------------------------------------------------------------------
%% @doc Behaviour to implement for grpc service smartspeech.synthesis.v1.SmartSpeech.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:20+00:00 and should not be modified manually

-module(grpcbox_sber_tts_synthesis_bhvr).

%% @doc 
-callback synthesize(grpcbox_synthesis_pb:synthesis_request(), grpcbox_stream:t()) ->
    ok | grpcbox_stream:grpc_error_response().

%% @doc Unary RPC
-callback async_synthesize(ctx:ctx(), grpcbox_synthesis_pb:async_synthesis_request()) ->
    {ok, grpcbox_synthesis_pb:task(), ctx:ctx()} | grpcbox_stream:grpc_error_response().

