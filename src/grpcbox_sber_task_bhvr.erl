%%%-------------------------------------------------------------------
%% @doc Behaviour to implement for grpc service smartspeech.task.v1.SmartSpeech.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:21+00:00 and should not be modified manually

-module(grpcbox_sber_task_bhvr).

%% @doc Unary RPC
-callback get_task(ctx:ctx(), grpcbox_task_pb:get_task_request()) ->
    {ok, grpcbox_task_pb:task(), ctx:ctx()} | grpcbox_stream:grpc_error_response().

%% @doc Unary RPC
-callback cancel_task(ctx:ctx(), grpcbox_task_pb:cancel_task_request()) ->
    {ok, grpcbox_task_pb:task(), ctx:ctx()} | grpcbox_stream:grpc_error_response().

