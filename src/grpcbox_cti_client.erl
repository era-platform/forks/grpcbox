%%%-------------------------------------------------------------------
%% @doc Client module for grpc service Stc.SmartLogger.VoIPRecorder.CtiInterface.Grpc.SlCtiProtocolService.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:08+00:00 and should not be modified manually

-module(grpcbox_cti_client).

-compile(export_all).
-compile(nowarn_export_all).

-include_lib("grpcbox/include/grpcbox.hrl").

-define(is_ctx(Ctx), is_tuple(Ctx) andalso element(1, Ctx) =:= ctx).

-define(SERVICE, 'Stc.SmartLogger.VoIPRecorder.CtiInterface.Grpc.SlCtiProtocolService').
-define(PROTO_MODULE, 'grpcbox_CtiInterface_pb').
-define(MARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:encode_msg(I, T) end).
-define(UNMARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:decode_msg(I, T) end).
-define(DEF(Input, Output, MessageType), #grpcbox_def{service=?SERVICE,
                                                      message_type=MessageType,
                                                      marshal_fun=?MARSHAL_FUN(Input),
                                                      unmarshal_fun=?UNMARSHAL_FUN(Output)}).

%% @doc Unary RPC
-spec open(grpcbox_CtiInterface_pb:open_request()) ->
    {ok, grpcbox_CtiInterface_pb:open_response(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
open(Input) ->
    open(ctx:new(), Input, #{}).

-spec open(ctx:t() | grpcbox_CtiInterface_pb:open_request(), grpcbox_CtiInterface_pb:open_request() | grpcbox_client:options()) ->
    {ok, grpcbox_CtiInterface_pb:open_response(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
open(Ctx, Input) when ?is_ctx(Ctx) ->
    open(Ctx, Input, #{});
open(Input, Options) ->
    open(ctx:new(), Input, Options).

-spec open(ctx:t(), grpcbox_CtiInterface_pb:open_request(), grpcbox_client:options()) ->
    {ok, grpcbox_CtiInterface_pb:open_response(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
open(Ctx, Input, Options) ->
    grpcbox_client:unary(Ctx, <<"/Stc.SmartLogger.VoIPRecorder.CtiInterface.Grpc.SlCtiProtocolService/Open">>, Input, ?DEF(open_request, open_response, <<"Stc.SmartLogger.VoIPRecorder.CtiInterface.Grpc.OpenRequest">>), Options).

%% @doc 
-spec server_events(grpcbox_CtiInterface_pb:server_events_request()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
server_events(Input) ->
    server_events(ctx:new(), Input, #{}).

-spec server_events(ctx:t() | grpcbox_CtiInterface_pb:server_events_request(), grpcbox_CtiInterface_pb:server_events_request() | grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
server_events(Ctx, Input) when ?is_ctx(Ctx) ->
    server_events(Ctx, Input, #{});
server_events(Input, Options) ->
    server_events(ctx:new(), Input, Options).

-spec server_events(ctx:t(), grpcbox_CtiInterface_pb:server_events_request(), grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
server_events(Ctx, Input, Options) ->
    grpcbox_client:stream(Ctx, <<"/Stc.SmartLogger.VoIPRecorder.CtiInterface.Grpc.SlCtiProtocolService/ServerEvents">>, Input, ?DEF(server_events_request, server_event, <<"Stc.SmartLogger.VoIPRecorder.CtiInterface.Grpc.ServerEventsRequest">>), Options).

