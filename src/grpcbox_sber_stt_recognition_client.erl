%%%-------------------------------------------------------------------
%% @doc Client module for grpc service smartspeech.recognition.v1.SmartSpeech.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:19+00:00 and should not be modified manually

-module(grpcbox_sber_stt_recognition_client).

-compile(export_all).
-compile(nowarn_export_all).

-include_lib("grpcbox/include/grpcbox.hrl").

-define(is_ctx(Ctx), is_tuple(Ctx) andalso element(1, Ctx) =:= ctx).

-define(SERVICE, 'smartspeech.recognition.v1.SmartSpeech').
-define(PROTO_MODULE, 'grpcbox_recognition_pb').
-define(MARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:encode_msg(I, T) end).
-define(UNMARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:decode_msg(I, T) end).
-define(DEF(Input, Output, MessageType), #grpcbox_def{service=?SERVICE,
                                                      message_type=MessageType,
                                                      marshal_fun=?MARSHAL_FUN(Input),
                                                      unmarshal_fun=?UNMARSHAL_FUN(Output)}).

%% @doc 
-spec recognize() ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
recognize() ->
    recognize(ctx:new(), #{}).

-spec recognize(ctx:t() | grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
recognize(Ctx) when ?is_ctx(Ctx) ->
    recognize(Ctx, #{});
recognize(Options) ->
    recognize(ctx:new(), Options).

-spec recognize(ctx:t(), grpcbox_client:options()) ->
    {ok, grpcbox_client:stream()} | grpcbox_stream:grpc_error_response().
recognize(Ctx, Options) ->
    grpcbox_client:stream(Ctx, <<"/smartspeech.recognition.v1.SmartSpeech/Recognize">>, ?DEF(recognition_request, recognition_response, <<"smartspeech.recognition.v1.RecognitionRequest">>), Options).

%% @doc Unary RPC
-spec async_recognize(grpcbox_recognition_pb:async_recognize_request()) ->
    {ok, grpcbox_recognition_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
async_recognize(Input) ->
    async_recognize(ctx:new(), Input, #{}).

-spec async_recognize(ctx:t() | grpcbox_recognition_pb:async_recognize_request(), grpcbox_recognition_pb:async_recognize_request() | grpcbox_client:options()) ->
    {ok, grpcbox_recognition_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
async_recognize(Ctx, Input) when ?is_ctx(Ctx) ->
    async_recognize(Ctx, Input, #{});
async_recognize(Input, Options) ->
    async_recognize(ctx:new(), Input, Options).

-spec async_recognize(ctx:t(), grpcbox_recognition_pb:async_recognize_request(), grpcbox_client:options()) ->
    {ok, grpcbox_recognition_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
async_recognize(Ctx, Input, Options) ->
    grpcbox_client:unary(Ctx, <<"/smartspeech.recognition.v1.SmartSpeech/AsyncRecognize">>, Input, ?DEF(async_recognize_request, task, <<"smartspeech.recognition.v1.AsyncRecognizeRequest">>), Options).

