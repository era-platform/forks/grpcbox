%%%-------------------------------------------------------------------
%% @doc Behaviour to implement for grpc service vosk.stt.v1.SttService.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:11+00:00 and should not be modified manually

-module(grpcbox_vosk_stt_bhvr).

%% @doc 
-callback streaming_recognize(reference(), grpcbox_stream:t()) ->
    ok | grpcbox_stream:grpc_error_response().

