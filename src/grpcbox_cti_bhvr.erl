%%%-------------------------------------------------------------------
%% @doc Behaviour to implement for grpc service Stc.SmartLogger.VoIPRecorder.CtiInterface.Grpc.SlCtiProtocolService.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:08+00:00 and should not be modified manually

-module(grpcbox_cti_bhvr).

%% @doc Unary RPC
-callback open(ctx:ctx(), grpcbox_CtiInterface_pb:open_request()) ->
    {ok, grpcbox_CtiInterface_pb:open_response(), ctx:ctx()} | grpcbox_stream:grpc_error_response().

%% @doc 
-callback server_events(grpcbox_CtiInterface_pb:server_events_request(), grpcbox_stream:t()) ->
    ok | grpcbox_stream:grpc_error_response().

