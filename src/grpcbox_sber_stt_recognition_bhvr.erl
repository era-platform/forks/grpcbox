%%%-------------------------------------------------------------------
%% @doc Behaviour to implement for grpc service smartspeech.recognition.v1.SmartSpeech.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:19+00:00 and should not be modified manually

-module(grpcbox_sber_stt_recognition_bhvr).

%% @doc 
-callback recognize(reference(), grpcbox_stream:t()) ->
    ok | grpcbox_stream:grpc_error_response().

%% @doc Unary RPC
-callback async_recognize(ctx:ctx(), grpcbox_recognition_pb:async_recognize_request()) ->
    {ok, grpcbox_recognition_pb:task(), ctx:ctx()} | grpcbox_stream:grpc_error_response().

