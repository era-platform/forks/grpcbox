%%%-------------------------------------------------------------------
%% @doc Client module for grpc service smartspeech.task.v1.SmartSpeech.
%% @end
%%%-------------------------------------------------------------------

%% this module was generated on 2023-09-25T11:35:21+00:00 and should not be modified manually

-module(grpcbox_sber_task_client).

-compile(export_all).
-compile(nowarn_export_all).

-include_lib("grpcbox/include/grpcbox.hrl").

-define(is_ctx(Ctx), is_tuple(Ctx) andalso element(1, Ctx) =:= ctx).

-define(SERVICE, 'smartspeech.task.v1.SmartSpeech').
-define(PROTO_MODULE, 'grpcbox_task_pb').
-define(MARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:encode_msg(I, T) end).
-define(UNMARSHAL_FUN(T), fun(I) -> ?PROTO_MODULE:decode_msg(I, T) end).
-define(DEF(Input, Output, MessageType), #grpcbox_def{service=?SERVICE,
                                                      message_type=MessageType,
                                                      marshal_fun=?MARSHAL_FUN(Input),
                                                      unmarshal_fun=?UNMARSHAL_FUN(Output)}).

%% @doc Unary RPC
-spec get_task(grpcbox_task_pb:get_task_request()) ->
    {ok, grpcbox_task_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
get_task(Input) ->
    get_task(ctx:new(), Input, #{}).

-spec get_task(ctx:t() | grpcbox_task_pb:get_task_request(), grpcbox_task_pb:get_task_request() | grpcbox_client:options()) ->
    {ok, grpcbox_task_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
get_task(Ctx, Input) when ?is_ctx(Ctx) ->
    get_task(Ctx, Input, #{});
get_task(Input, Options) ->
    get_task(ctx:new(), Input, Options).

-spec get_task(ctx:t(), grpcbox_task_pb:get_task_request(), grpcbox_client:options()) ->
    {ok, grpcbox_task_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
get_task(Ctx, Input, Options) ->
    grpcbox_client:unary(Ctx, <<"/smartspeech.task.v1.SmartSpeech/GetTask">>, Input, ?DEF(get_task_request, task, <<"smartspeech.task.v1.GetTaskRequest">>), Options).

%% @doc Unary RPC
-spec cancel_task(grpcbox_task_pb:cancel_task_request()) ->
    {ok, grpcbox_task_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
cancel_task(Input) ->
    cancel_task(ctx:new(), Input, #{}).

-spec cancel_task(ctx:t() | grpcbox_task_pb:cancel_task_request(), grpcbox_task_pb:cancel_task_request() | grpcbox_client:options()) ->
    {ok, grpcbox_task_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
cancel_task(Ctx, Input) when ?is_ctx(Ctx) ->
    cancel_task(Ctx, Input, #{});
cancel_task(Input, Options) ->
    cancel_task(ctx:new(), Input, Options).

-spec cancel_task(ctx:t(), grpcbox_task_pb:cancel_task_request(), grpcbox_client:options()) ->
    {ok, grpcbox_task_pb:task(), grpcbox:metadata()} | grpcbox_stream:grpc_error_response().
cancel_task(Ctx, Input, Options) ->
    grpcbox_client:unary(Ctx, <<"/smartspeech.task.v1.SmartSpeech/CancelTask">>, Input, ?DEF(cancel_task_request, task, <<"smartspeech.task.v1.CancelTaskRequest">>), Options).

